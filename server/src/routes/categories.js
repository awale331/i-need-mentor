const router = require('express').Router();
const category = require('../controller/categories');
const authTokenValidator = require('../middleware/authTokenValidator');
const accessControl = require('../middleware/accessControl');

router.get('/', category.index);
router.get('/:parent', category.getChildren);
router.post('/', authTokenValidator, accessControl, category.create);
router.delete('/:id', authTokenValidator, accessControl, category.remove);
router.put('/:id', authTokenValidator, accessControl, category.update);

module.exports = router;
