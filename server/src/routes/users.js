const router = require('express').Router();
const mentors = require('../controller/mentors');
const admins = require('../controller/admins');
const changeUserRole = require('../controller/changeUserRole');
const validateToken = require('../middleware/authTokenValidator');
const validateBody = require('../middleware/userValidator');
const accessControl = require('../middleware/accessControl');
const changePassword = require('../controller/changePassword');
const validatePassword = require('../middleware/validatePassword');
const feedback = require('../controller/feedback');
const search = require('../controller/searchMentor');
const workshop = require('../controller/workshop');
const phone = require('../controller/phonenumber');
const validatePhone = require('../middleware/phoneValidation');

router.get('/mentors', validateToken, mentors.index);
router.post('/mentors', validateToken, accessControl, validateBody, mentors.create);
router.post('/mentors/:id/details', validateToken, accessControl, mentors.addDetails);
router.get('/mentors/:id/details', mentors.getDetails);
router.put('/mentors/:id', validateToken, accessControl, mentors.update);
router.put('/mentors/:id/details', validateToken, accessControl, mentors.updateDetails);
router.delete('/mentors/:id', validateToken, accessControl, mentors.remove);

router.get('/admins', admins.index);
router.get('/admins/:id', validateToken, accessControl, admins.findOne);
router.post('/admins', validateToken, accessControl, validateBody, admins.create);
router.put('/admins/:id', validateToken, accessControl, admins.update);
router.delete('/admins/:id', validateToken, accessControl, admins.remove);

router.post('/feedback/:id', validateToken, feedback.create);
router.delete('/feedback/:id', validateToken, feedback.remove);

router.put('/changepassword', validateToken, validatePassword, changePassword);
router.put('/role', validateToken, changeUserRole);


router.post('/workshop/:id',validateToken, accessControl, workshop.signup);
router.get('/workshop/export-to-excel',validateToken, workshop.exportToExcel);   

router.post('/add-phone/:id', validateToken, accessControl, validatePhone, phone);


module.exports = router;
