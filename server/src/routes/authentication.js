const router = require('express').Router();
const users = require('../controller/users');
const { verifyEmail, resendVerificationEmail } = require('../controller/emailVerification');
const validateBody = require('../middleware/userValidator');
const validateToken = require('../middleware/authTokenValidator');
const login = require('../controller/loginController');
const logout = require('../controller/logout');
const google = require('../controller/googleLoginController');
const search = require('../controller/searchMentor');
const rotateToken = require('../controller/rotateToken');
const facebook = require('../controller/facebookLoginController');
const { sendResetToken, updatePassword } = require('../controller/resetPassword');
const validatePassword = require('../middleware/validatePassword');

router.route('/signup').post(validateBody, users.create);
router.route('/verifyemail').get(verifyEmail);
router.route('/resendverification').post(resendVerificationEmail);
router.post('/login', login);
router.post('/login/admin', login);
router.post('/logout', validateToken, logout);
router.route('/google').post(google);
router.route('/facebook').post(facebook);
router.post('/token', rotateToken);

module.exports = router;