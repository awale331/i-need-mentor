const mongoose = require('mongoose');
const Mentor = require('./mentors');
const Student = require('./students');

const feedbackSchema = mongoose.Schema({
  feedback: { type: String },
  correspondent: { type: mongoose.Schema.Types.ObjectId },
});


const userSchema = mongoose.Schema({
  fullName: { type: String, required: true, trim: true },
  role: [{ type: String }],
  email: { type: String, required: true, unique: true, trim: true },
  password: { type: String, select: false }, // select:false excludes the field by default
  isVerified: { type: Boolean, default: false },
  // photo: { type: String },
  mentor: { type: mongoose.Schema.Types.ObjectId, ref: Mentor },
  student: { type: mongoose.Schema.Types.ObjectId, ref: Student },
  feedbacks: [feedbackSchema],
  googleID: { type: String },
<<<<<<< HEAD
=======
  facebookID: { type: String },
  tags: [{ type: String }],
  workshop: [{ type: String }],
  // session: { }
});

userSchema.index({ fullName: 'text', tags: 'text' });

userSchema.pre('save', async function (next) {
  try {
    if (!this.isModified('password')) return next(); // only hash the password if it has been modified (or is new)
    this.password = await bcrypt.hash(this.password, 10);
    next();
  } catch (err) {
    next(err);
  }
>>>>>>> 6986db7b68f46bd7061173f6e75f8df6120b5f85
});

userSchema
  .virtual('feedbackPercent')
  .get(function () {
    const total = this.feedbacks.length;
    const pos = this.feedbacks.filter((feedback) => feedback.feedback === 'positive');
    const percentage = (pos.length / total) * 100;
    return percentage;
  });

userSchema.set('toObject', { getters: true });

userSchema.methods.toJSON = function () {
  var obj = this.toObject()
  delete obj.password
  delete obj.refreshToken
  return obj
}
module.exports = mongoose.model('User', userSchema);
