const mongoose = require('mongoose');
const User = require('../models/users');

const refreshTokenSchema = mongoose.Schema({
  createdAt: { type: Date, expires: process.env.REFRESH_TOKEN_EXP, default: Date.now },
  user: { type: mongoose.Schema.Types.ObjectId, ref: User, required: true },
  refreshToken: { type: String },
});

module.exports = mongoose.model('RefreshToken', refreshTokenSchema);
