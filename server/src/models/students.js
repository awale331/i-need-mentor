const mongoose = require('mongoose');
const Category = require('./categories');

const studentSchema = mongoose.Schema({
  interest: [{ type: String, required: true, trim: true }],
  category: { type: mongoose.Schema.Types.ObjectId, ref: Category },
});

module.exports = mongoose.model('Student', studentSchema);

