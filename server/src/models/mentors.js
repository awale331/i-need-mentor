const mongoose = require('mongoose');
const Student = require('./students');
const Category = require('./categories');

const mentorSchema = mongoose.Schema({
  phone: { type: String, required: true, trim: true, unique: true },
  facebookUrl: { type: String, trim: true },
  linkedinUrl: { type: String, trim: true },
  twitterUrl: { type: String, trim: true },
  skills: [String],
  available: { type: Boolean, default: true },
  category: { type: mongoose.Schema.Types.ObjectId, ref: Category },
  experience: { type: String, required: true },
  students: [{ type: mongoose.Schema.Types.ObjectId, ref: Student }],
});

mentorSchema.index({ skills: 'text' });
module.exports = mongoose.model('Mentor', mentorSchema);
