const User = require('../models/users');
const Token = require('../models/refreshTokens');
const Student = require('../models/students');
const createStudent = require('../dbQuery/createStudent');
const createError = require('../helper/createError');
const jwt = require('jsonwebtoken');
const tokenGenerator = require('../helper/tokenGenerator');
const email_verify = require('../controller/emailVerification');


exports.create = async (req, res, next) => {
  try {
    let email = await User.findOne({ email: req.body.email }).select('+password');
    if (email) {
      if (email.password == undefined) {
        createStudent.updateStudent(req, res, next);
      }
      else {
        next(createError(400, 'Email already exists'));
      }
    }
    else {
      createStudent.createStudents(req, res, next);
    }
  }
  catch (err) {
    next(err)
  }
}


exports.confirmation = async (req, res, next) => {
  try {
    if (req.query.token) {
      decoded = await jwt.verify(req.query.token, process.env.JWT_SECRET);
      if (decoded.scope !== 'email') {
        return next(createError(401, 'Token out of scope'));
      }
      const email = decoded.data;
      let user = await User.findOne({ email: email });
      if (user.isVerified)
        return res.status(200).json({
          "success": true,
          "payload": "Already Verified"
        });
      user.isVerified = true;
      await user.save();
      res.status(200).json({
        "success": true,
        "payload": "User Verififed"
      });
    }
  }
  catch (err) {
    next(err);
  }
}

exports.details = async (req, res, next) => {
  try {
    let user = await User.findOne({ email: res.locals.email });
    const student = new Student({
      category: req.body.category,
    });
    await student.save();
    user.student = student.id;
    await user.save();
    res.status(200).json({
      "success": true,
      "payload": student
    });
  }
  catch (err) {
    next(err);
  }
}


exports.resendVerification = async (req, res, next) => {
  try {
    let user = await User.findOne({ email: req.body.email })
    if (user.isVerified)
      return res.status(200).json({
        "success": true,
        "payload": "Already verified"
      })
    const access_token = tokenGenerator.accessToken(user.email, 'email');
    const host = req.get('host');
    await email_verify.verifyEmail(user.email, user.fullName, host, access_token);
    res.status(200).json({
      "success": true,
      "payload": "Verification Email Send"
    });
  }
  catch (err) {
    next(err)
  }
}


exports.update = async (req, res, next) => {
  try {
    let user = await User.findOne({ _id: req.params.id });
    let category = await Student.findByIdAndUpdate(user.student, req.body, { new: true });
    res.status(200).json({
      "success": true,
      "payload": category
    })
  }
  catch (err) {
    next(err);
  }
}


exports.delete = async (req, res, next) => {
  const user = await User.findByIdAndDelete(req.params.id);
  Student.findByIdAndDelete(user.student);
  await Token.deleteMany({ user: user.id });
  res.status(200).json({
    "success": true,
    "payload": "Successfully deleted"
  })
}