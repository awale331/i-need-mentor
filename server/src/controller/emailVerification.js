const jwt = require('jsonwebtoken');
const User = require('../models/users');
const { tokenGenerator } = require('../helper/tokenGenerator');
const sendEmail = require('../helper/sendEmail');
const createError = require('../helper/createError');

const resendVerificationEmail = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) return next(createError(400, 'user does not exist'));
    if (user.isVerified) {
      return res.status(200).json({
        success: true,
        payload: 'email already verified',
      });
    }

    const accountVerificationToken = tokenGenerator({ email: req.body.email }, 'accountVerification', process.env.EMAIL_TOKEN_EXP);
    const tokenValidity = process.env.EMAIL_TOKEN_EXP.slice(0, -1);
    await sendEmail({
      to: req.body.email,
      subject: 'Account Verification',
      template: 'mentors',
      name: req.body.fullName,
      host: req.get('host'),
      token: accountVerificationToken,
      validity: tokenValidity,
    });
  } catch (err) {
    next(err);
  }
};

const verifyEmail = async (req, res, next) => {
  try {
    if (req.query.token) {
      const { scope, data } = await jwt.verify(req.query.token, process.env.JWT_SECRET);
      if (scope !== 'accountVerification') {
        return res.redirect(`${process.env.REACT_SERVER}/login?emailverification=fail`);
      }

      const user = await User.findOne({ email: data.email });
      if (user.isVerified) {
        return res.redirect(`${process.env.REACT_SERVER}/login?emailverification=success`);
      }
      user.isVerified = true;
      await user.save();
      return res.redirect(`${process.env.REACT_SERVER}/login?emailverification=success`);
    }
    res.redirect(`${process.env.REACT_SERVER}/login`);
  } catch (err) {
    res.redirect(`${process.env.REACT_SERVER}/login?emailverification=fail`);
  }
};

module.exports = { verifyEmail, resendVerificationEmail };
