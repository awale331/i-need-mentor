const bcrypt = require('bcrypt');
const User = require('../models/users');
const Mentor = require('../models/mentors');
const emailVerify = require('../controller/emailVerification');
const tokenGenerator = require('../helper/tokenGenerator');
const createError = require('../helper/createError');
const Token = require('../models/refreshTokens');

const index = async (req, res, next) => {
  try {
    let mentors;
    if (req.query.sort === 'alphabetically') {
      mentors = await User
        .find({ role: 'mentor', isVerified: true })
        .collation({ locale: 'en', strength: 2 }) // ignore case sensitivity
        .sort({ fullName: 1 });
    } else if (req.query.sort === 'recent') {
      mentors = await User
        .find({ role: 'mentor', isVerified: true })
        .sort({ _id: -1 });
    } else if (req.query.sort === 'oldest') {
      mentors = await User
        .find({ role: 'mentor', isVerified: true })
        .sort({ _id: 1 });
    } else {
      mentors = await User.find({ role: 'mentor' }).populate('mentor');
    }
    return res.status(200).json({
      success: true,
      payload: mentors,
    });
  } catch (err) {
    next(err);
  }
};

const sendConfirmationMail = async (req, res, next) => {
  try {
    const host = req.get('host');
    const accessToken = tokenGenerator.accessToken(req.body.email, 'email');
    await emailVerify.verifyEmail(req.body.email, req.body.fullName, host, accessToken);
  } catch (err) {
    next(err);
  }
};

const create = async (req, res, next) => {
  try {
    const email = await User.findOne({ email: req.body.email });
    if (email) {
      next(createError(400, 'An account with the email already exists'));
    } else {
      sendConfirmationMail(req, res, next);
      req.body.password = await bcrypt.hash(req.body.password, 10);
      req.body.role = ['mentor'];
      const mentor = await User.create(req.body);
      return res.status(201).json({
        success: true,
        payload: mentor,
      });
    }
  } catch (err) {
    next(err);
  }
};

const addDetails = async (req, res, next) => {
  try {
    const mentorDetails = await Mentor.create(req.body);
    await User.findByIdAndUpdate(req.params.id, { mentor: mentorDetails.id });
    res.status(200).json(mentorDetails);
  } catch (err) {
    next(err);
  }
};

const getDetails = async (req, res, next) => {
  try {
    const mentor = await User.findById(req.params.id).populate('mentor');
    res.status(200).json(mentor);
  } catch (err) {
    next(err);
  }
};

const update = async (req, res, next) => {
  try {
    if (res.locals.role.includes === 'admin') {
      if (req.body.password) { // hash password if it exists in req.body
        req.body.password = await bcrypt.hash(req.body.password, 10);
      }
      const user = await User.findByIdAndUpdate(res.locals.id, req.body, { new: true });
      return res.status(200).json({
        success: true,
        payload: user,
      });
    }
    next(createError(401, 'Not authorized'));
  } catch (err) {
    next(err);
  }
};

const updateDetails = async (req, res, next) => {
  try {
    const mentor = await User.findById(req.params.id);
    const mentorDetails = await Mentor.findByIdAndUpdate(mentor.mentor, req.body, { new: true });
    res.status(200).json({
      success: true,
      payload: mentorDetails,
    });
  } catch (err) {
    next(err);
  }
};

const remove = async (req, res, next) => {
  try {
    const mentor = await User.findByIdAndRemove(req.params.id);
    Mentor.findByIdAndRemove(mentor.mentor);
    await Token.deleteMany({ user: mentor.id });
    res.status(200).json({
      success: true,
      payload: mentor,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  index,
  create,
  addDetails,
  getDetails,
  update,
  updateDetails,
  remove,
};
