const bcrypt = require('bcrypt');
const User = require('../models/users');
const createError = require('../helper/createError');
const tokenGenerator = require('../helper/tokenGenerator');
const Token = require('../models/refreshTokens');

module.exports = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email }).select('+password');
    if (user) {
      if (req.path === '/login/admin' && !user.role.includes('admin')) return next(createError(401, 'Not authorized'));
      if (!user.isVerified) return next(createError(401, 'Please verify your account first.'));
      if (await user.passwordMatch(req.body.password)) {
        const accessToken = tokenGenerator.accessToken({
          email: user.email,
          id: user.id,
          role: user.role,
          isVerified: user.isVerified,
        }, 'auth');
        const refreshToken = tokenGenerator.refreshToken(user.id, 'auth');
        await Token.create({ user: user.id, refreshToken });

        const payload = user.toJSON();
        delete payload.password; // exclude password from response

        return res.status(200).json({
          success: true,
          accessToken,
          refreshToken,
          payload,
        });
      }
      next(createError(400, 'Invalid Email or Password'));
    } else {
      next(createError(400, 'Invalid Email or Password'));
    }
  } catch (err) {
    next(err);
  }
};