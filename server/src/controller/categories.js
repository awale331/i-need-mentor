const { arrayToTree } = require('performant-array-to-tree');
const mongoose = require('mongoose');
const Category = require('../models/categories');

const index = async (req, res, next) => {
  try {
    const categories = await Category.find().lean();
    const categoriesTree = arrayToTree(categories, { dataField: null, id: 'name', parentId: 'parent' });
    res.status(200).json({
      success: true,
      payload: categoriesTree,
    });
  } catch (err) {
    next(err);
  }
};

const getChildren = async (req, res, next) => {
  try {
    const parent = req.params.parent === 'null' ? null : req.params.parent; // if parameter is 'null', convert the type from string to null
    const subcategories = await Category.find({ parent });
    res.status(200).json({
      success: true,
      payload: subcategories,
    });
  } catch (err) {
    next(err);
  }
};

const create = async (req, res, next) => {
  try {
    const category = await Category.create(req.body);
    res.status(201).json({
      success: true,
      payload: category,
    });
  } catch (err) {
    next(err);
  }
};

const remove = async (req, res, next) => {
  try {
    const aggregate = Category.aggregate([
      { $match: { _id: mongoose.Types.ObjectId(req.params.id) } },
      { $project: { _id: 1, name: 1, parent: 1 } },
    ]);
    const category = await aggregate.graphLookup({  // Get the category and all its children
      from: 'categories',
      startWith: '$name',
      connectFromField: 'name',
      connectToField: 'parent',
      as: 'children',
    });

    const categoriesToBeDeleted = [category[0].name]; // categories and their children to be deleted

    category[0].children.forEach((child) => {
      categoriesToBeDeleted.push(child.name);
    });
    const result = await Category.deleteMany({ name: { $in: categoriesToBeDeleted } });

    res.status(200).json({
      success: true,
      payload: {
        totalDeleted: result.deletedCount,
      },
    });
  } catch (err) {
    next(err);
  }
};

const update = async (req, res, next) => {
  try {
    const category = await Category.findByIdAndUpdate(req.params.id, req.body);
    let totalModified = 1;
    if (req.body.name && req.body.name !== category.name) {
      const children = await Category.updateMany(
        { parent: category.name },
        { parent: req.body.name },
      );
      totalModified += children.nModified;
    }

    return res.status(200).json({
      success: true,
      payload: {
        totalModified,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  index,
  create,
  remove,
  update,
  getChildren,
};
