const bcrypt = require('bcrypt');
const User = require('../models/users');
const emailVerify = require('../controller/emailVerification');
const tokenGenerator = require('../helper/tokenGenerator');
const createError = require('../helper/createError');
const Token = require('../models/refreshTokens');

const index = async (req, res, next) => {
  try {
    const admins = await User.find({ role: 'admin' });
    return res.status(200).json({
      success: true,
      payload: admins,
    });
  } catch (err) {
    next(err);
  }
};

const findOne = async (req, res, next) => {
  try {
    const admin = await User.findById(req.params.id);
    return res.status(200).json({
      success: true,
      payload: admin,
    });
  } catch (err) {
    next(err);
  }
}

const sendConfirmationMail = async (req, res, next) => {
  try {
    const host = req.get('host');
    const accessToken = tokenGenerator.accessToken(req.body.email, 'email');
    await emailVerify.verifyEmail(req.body.email, req.body.fullName, host, accessToken);
  } catch (err) {
    next(err);
  }
};

const create = async (req, res, next) => {
  try {
    const email = await User.findOne({ email: req.body.email });
    if (email) {
      next(createError(400, 'Email already exists'));
    } else {
      sendConfirmationMail(req, res, next);
      req.body.password = await bcrypt.hash(req.body.password, 10);
      req.body.role = ['admin'];
      const admin = await User.create(req.body);
      return res.status(201).json({
        success: true,
        payload: admin,
      });
    }
  } catch (err) {
    next(err);
  }
};

const update = async (req, res, next) => {
  try {
    if (req.body.password) {
      req.body.password = await bcrypt.hash(req.body.password, 10);
    }
    const admin = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
    return res.status(200).json({
      success: true,
      payload: admin,
    });
  } catch (err) {
    next(err);
  }
};

const remove = async (req, res, next) => {
  try {
    const admin = await User.findByIdAndRemove(req.params.id);
    await Token.deleteMany({ user: admin.id });
    return res.status(200).json({
      success: true,
      payload: admin,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  index,
  create,
  update,
  findOne,
  remove,
};
