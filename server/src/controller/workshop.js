const fs = require('fs');
const path = require('path');
const json2csv = require('json2csv').parse;
const url = require('url');
const User = require('../models/users');
const sendEmail = require('../helper/sendEmail');

const signup = async (req, res, next) => {
  try {
    const user = await User.findById(res.locals.id);
    if (user.workshop.includes(req.body.workshop)) {
      return res.status(400).json({
        success: false,
        message: 'Already registered for workshop',
      });
    }
    user.workshop.push(req.body.workshop);
    user.save();

    await sendEmail({
      to: user.email,
      subject: 'Workshop Registration',
      template: 'workshop',
      name: user.fullName,
      host: req.get('host'),
    });

    return res.status(200).json({
      success: true,
      payload: user,
    });
  } catch (err) {
    next(err);
  }
};

const exportToExcel = async (req, res, next) => {
  const fileName = "export.csv";
  const fields = ["fullName", "email", "workshop"];
  const students = await User.find({
    role: "student",
    workshop: { $exists: true, $ne: [] }
  });
  let csv;
  try {
    csv = await json2csv(students, { fields });
  } catch (err) {
    next(err);
  }
  const filePath = path.join(__dirname, "..", "public", fileName);
  fs.writeFile(filePath, csv, function (err) {
    if (err) {
      next(err);
    } else {
      setTimeout(function () {
        fs.unlinkSync(filePath); // delete this file after 1 hour
      }, 3600000);
      const protocol = req.protocol;
      const host = url.parse(req.hostname).pathname;
      const port = process.env.SERVER_PORT;
      return res.status(200).redirect(`${protocol}://${host}:${port}/export.csv`);
    }
  });
};

module.exports = {
  signup,
  exportToExcel,
};
