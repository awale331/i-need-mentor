const User = require('../models/users');
const { tokenGenerator } = require('../helper/tokenGenerator');
const createError = require('../helper/createError');
const Token = require('../models/refreshTokens');
const sendEmail = require('../helper/sendEmail');

const index = async (req, res, next) => {
  try {
    const perPage = Number(process.env.RESULTS_PER_PAGE);
    const page = Number(req.query.page);
    let users;

    if (req.query.sort === 'alphabetically') {
      users = await User
        .find({ role: req.query.role })
        .collation({ locale: 'en', strength: 2 }) // ignore case sensitivity
        .sort({ fullName: 1 })
        .limit(perPage)
        .skip(perPage * page);
    } else if (req.query.sort === 'recent') {
      users = await User
        .find({ role: req.query.role })
        .sort({ _id: -1 })
        .limit(perPage)
        .skip(perPage * page);
    } else if (req.query.sort === 'oldest') {
      users = await User
        .find({ role: req.query.role })
        .sort({ _id: 1 })
        .limit(perPage)
        .skip(perPage * page);
    } else {
      users = await User
        .find({ role: req.query.role })
        .limit(perPage)
        .skip(perPage * page);
    }

    const numOfUsers = await User.count({ role: req.query.role });

    return res.status(200).json({
      success: true,
      payload: {
        users,
        numOfPages: Math.ceil(numOfUsers / perPage),
      },
    });
  } catch (err) {
    next(err);
  }
};

const findById = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id || res.locals.id);
    return res.status(200).json({
      success: true,
      payload: user,
    });
  } catch (err) {
    next(err);
  }
};

const create = async (req, res, next) => {
  try {
    const account = await User.findOne({ email: req.body.email });
    if (account) {
      next(createError(400, 'An account with the email already exists'));
    } else {
      const accountVerificationToken = tokenGenerator({ email: req.body.email }, 'accountVerification', process.env.EMAIL_TOKEN_EXP);
      const tokenValidity = process.env.EMAIL_TOKEN_EXP.slice(0, -1);
      await sendEmail({
        to: req.body.email,
        subject: 'Account Verification',
        template: 'mentors',
        name: req.body.fullName,
        host: req.get('host'),
        token: accountVerificationToken,
        validity: tokenValidity,
      });

      const user = await User.create(req.body);
      return res.status(201).json({
        success: true,
        payload: user,
      });
    }
  } catch (err) {
    next(err);
  }
};

const update = async (req, res, next) => {
  try {
    if (!res.locals.role.includes('admin')) { // allow only admins to directly modify password
      delete req.body.password;
    }

    const user = await User.findByIdAndUpdate(res.locals.id, req.body, { new: true });
    return res.status(200).json({
      success: true,
      payload: user,
    });

  } catch (err) {
    next(err);
  }
};

const remove = async (req, res, next) => {
  try {
    const user = await User.findByIdAndRemove(req.params.id);
    await Token.deleteMany({ user: user.id });
    res.status(200).json({
      success: true,
      payload: user,
    });
  } catch (err) {
    next(err);
  }
};

const setAvatar = async (req, res, next) => {
  try {
    if (req.file) {
      const avatar = await User.findByIdAndUpdate(res.locals.id, { avatar: `avatar/${req.file.filename}` }, { new: true });
      return res.status(200).json({
        success: true,
        payload: avatar,
      });
    }
    return next(createError(400, 'avatar image required'));
  } catch (err) {
    next(err);
  }
};

module.exports = {
  index,
  findById,
  create,
  update,
  remove,
  setAvatar,
};
