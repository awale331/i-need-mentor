const User = require("../models/users");

module.exports = async (req, res, next) => {
  try {
    const user = await User.findById(res.locals.id);
    user.phone = req.body.phone;
    await user.save();
    return res.status(200).json({
      success: true,
      payload: user
    });
  } catch (err) {
    next(err);
  }
};
