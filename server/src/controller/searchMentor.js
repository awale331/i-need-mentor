const User = require('../models/users');
const Mentor = require('../models/mentors');

module.exports = async (req, res, next) => {
  try {
    const perPage = Number(process.env.RESULTS_PER_PAGE);
    const page = Number(req.query.page);

    const matchedMentors = await User
      .find({ role: 'mentor' })
      .or([
        { fullName: { $regex: `.*${req.query.q}.*`, $options: 'i' } },
        { tags: { $regex: `.*${req.query.q}.*`, $options: 'i' } },
      ])
      .limit(perPage)
      .skip(perPage * page);

    const numOfMatchedDocs = await User
      .find({ role: 'mentor' })
      .or([
        { fullName: { $regex: `.*${req.query.q}.*`, $options: 'i' } },
        { tags: { $regex: `.*${req.query.q}.*`, $options: 'i' } },
      ])
      .count();

    return res.status(200).json({
      success: true,
      payload: {
        matchedMentors,
        numOfPages: Math.ceil(numOfMatchedDocs / perPage),
      },
    });
  } catch (err) {
    next(err);
  }
};
