const jwt = require('jsonwebtoken');
const createError = require('../helper/createError');
const Token = require('../models/refreshTokens');
const User = require('../models/users');
const tokenGenerator = require('../helper/tokenGenerator');

module.exports = async (req, res, next) => {
  try {
    if (req.body.refreshToken) {
      const { data } = jwt.verify(req.body.refreshToken, process.env.JWT_SECRET);
      const token = await Token.findOne({ refreshToken: req.body.refreshToken });
      const user = await User.findById(data);
      if (token) { // to check if user has logged out or the token has been removed from whitelist.
        const accessToken = tokenGenerator.accessToken({
          email: user.email,
          id: user.id,
          role: user.role,
          isVerified: user.isVerified,
        }, 'auth');
        const refreshToken = tokenGenerator.refreshToken(user.id, 'auth');
        await Token.create({ user: user.id, refreshToken });
        return res.status(200).json({
          success: true,
          accessToken,
          refreshToken,
        });
      }
      return next(createError(401, 'Unauthorized'));
    }
    next(createError(401, 'Refresh token is required in the body.'));
  } catch (err) {
    if (err.name === 'TokenExpiredError' || err.name === 'JsonWebTokenError') {
      err.status = 401;
    }
    next(err);
  }
};
