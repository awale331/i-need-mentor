const Token = require('../models/refreshTokens');
const createError = require('../helper/createError');

module.exports = async (req, res, next) => {
  try {
    if (req.body.refreshToken) {
      await Token.findOneAndDelete({ refreshToken: req.body.refreshToken });
      return res.status(200).json({
        success: true,
        payload: {}
      });
    }
    next(createError(401, 'Refresh token is required in the body.'));
  } catch (err) {
    next(err);
  }
};
