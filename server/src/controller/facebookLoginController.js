const request = require("request-promise-native");
const User = require("../models/users");
const Token = require("../models/refreshTokens");
const tokenGenerator = require("../helper/tokenGenerator");

module.exports = async (req, res, next) => {
  try {
    const access_token = req.body.access_token;
    let response = await request.get(
      `https://graph.facebook.com/v5.0/me?fields=id,name,email&access_token=${access_token}`
    );

    data = JSON.parse(response);
    let accessToken = null;
    let refreshToken = null;
    let user = await User.findOne({ email: data.email });
    if (user) {
      accessToken = await tokenGenerator.accessToken(
        {
          email: user.email,
          id: user.id,
          role: user.role,
          isVerified: user.isVerified
        },
        "auth"
      );
      refreshToken = await tokenGenerator.refreshToken(user.id, "auth");
      await Token.create({ user: user.id, refreshToken });
      if (user.facebookID == undefined) {
        user.facebookID = data.id;
        user.isVerified = true;
        await user.save();
      }
    } else {
      data = {
        fullName: data.name,
        facebookID: data.id,
        role: ["student"],
        email: data.email,
        isVerified: true
      };
      user = await User.create(data);

      accessToken = await tokenGenerator.accessToken(
        {
          email: user.email,
          id: user._id,
          role: user.role,
          isVerified: user.isVerified
        },
        "auth"
      );
      refreshToken = await tokenGenerator.refreshToken(user.id, "auth");
      await Token.create({ user: user.id, refreshToken });
    }
    return res.status(200).json({
      status: true,
      accessToken: accessToken,
      refreshToken: refreshToken,
      payload: user
    });
  } catch (err) {
    next(err);
  }
};
