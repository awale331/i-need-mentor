const bcrypt = require('bcrypt');
const User = require('../models/users');
const createError = require('../helper/createError');

module.exports = async (req, res, next) => {
  try {
    const user = await User.findById(res.locals.id).select('+password');
    const passwordMatch = await bcrypt.compare(req.body.oldPassword, user.password);
    if (passwordMatch) {
      user.password = await bcrypt.hash(req.body.newPassword, 10);
      await user.save();
      delete user.password;
      return res.status(200).json({
        success: true,
        payload: user,
      });
    }
    next(createError(400, 'Invalid old password'));
  } catch (err) {
    next(err);
  }
};
