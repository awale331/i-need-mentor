const User = require('../models/users');
const createError = require('../helper/createError');

module.exports = async (req, res, next) => {
  try {
    // A mentor or student cannot change his role to admin
    if (!res.locals.role.includes('admin') && req.body.role.includes('admin')) {
      next(createError(400, 'A mentor or a student cannot change his role to admin'));
    }
    const user = await User.findByIdAndUpdate(res.locals.id, { role: req.body.role }, { new: true });
    return res.status(200).json({
      success: true,
      payload: user,
    });
  } catch (err) {
    next(err);
  }
}