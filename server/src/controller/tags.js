const fs = require('fs');
const Tags = require('../models/tags');
const createError = require('../helper/createError');

const create = async (req, res, next) => {
  try {
    const tags = await Tags.create(req.body);
    return res.status(201).json({
      success: true,
      payload: tags,
    });
  } catch (err) {
    next(err);
  }
};

const index = async (req, res, next) => {
  try {
    const tags = await Tags.find();
    return res.status(201).json({
      success: true,
      payload: tags,
    });
  } catch (err) {
    next(err);
  }
};

const update = async (req, res, next) => {
  try {
    const tags = await Tags.findByIdAndUpdate(req.params.id, req.body, { new: true });
    return res.status(201).json({
      success: true,
      payload: tags,
    });
  } catch (err) {
    next(err);
  }
};

const remove = async (req, res, next) => {
  try {
    const tags = await Tags.findByIdAndRemove(req.params.id);
    return res.status(201).json({
      success: true,
      payload: tags,
    });
  } catch (err) {
    next(err);
  }
};

const bulkUpload = async (req, res, next) => {
  try {
    if (!req.file) return (next(createError(400, 'csv file is required')));
    const fileContent = fs.readFileSync(req.file.path, 'utf8');
    fs.unlinkSync(req.file.path);

    const tagsArray = fileContent
      .split(/\n|,/)
      .map(tag => tag.trim())
      .filter(tag => tag !== '');

    const tags = tagsArray.map(tag => ({ name: tag }));

    try {
      await Tags.insertMany(tags, { ordered: false });
    } catch (err) {
      if (err.name !== 'BulkWriteError') {
        next(createError(500, err.message));
      }
    }

    return res.status(201).json({
      success: true,
      payload: tags,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  create,
  index,
  update,
  remove,
  bulkUpload,
};
