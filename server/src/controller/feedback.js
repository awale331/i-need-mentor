const User = require('../models/users');
const createError = require('../helper/createError');

const create = async (req, res, next) => {
  try {
    if (req.body.feedback !== 'positive' && req.body.feedback !== 'negative') {
      return next(createError(400, 'Request body must have a feedback with either "positive" or "negative".'));
    }

    const user = await User.findById(req.params.id);
    if (!user) {
      return next(createError(404, 'User with the specified id not found.'));
    }

    const feedback = user.feedbacks.find((feedbk) => feedbk.correspondent.toString() === res.locals.id);
    const newFeedback = {
      feedback: req.body.feedback,
      correspondent: res.locals.id,
    };

    if (feedback) {
      user.feedbacks.pull(feedback);
    }
    user.feedbacks.push(newFeedback);
    await user.save();

    return res.status(200).json({
      success: true,
      payload: newFeedback,
    });
  } catch (err) {
    next(err);
  }
};

const remove = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return next(createError(404, 'User with the specified id not found.'));
    }

    const feedback = user.feedbacks.find((feedbk) => feedbk.correspondent.toString() === res.locals.id);
    if (feedback) {
      user.feedbacks.pull(feedback);
      await user.save();
    }

    return res.status(200).json({
      success: true,
      payload: {},
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  create,
  remove,
};
