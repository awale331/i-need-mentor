const jwt = require('jsonwebtoken');
const createError = require('../helper/createError');

module.exports = async (req, res, next) => {
  let token = req.headers.authorization;
  try {
    if (token) {
      token = token.startsWith('Bearer') ? token.split(' ')[1] : token;
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      if (decoded.scope === 'auth' && decoded.data.id) { // ensure token is within auth scope and is indeed access token (and not refresh token)
        res.locals = decoded.data;
        return next();
      }
      return next(createError(401, 'Token out of scope'));
    }
    return next(createError(401, 'Access token is required'));
  } catch (err) {
    if (err.name === 'TokenExpiredError' || err.name === 'JsonWebTokenError') {
      err.status = 401;
    }
    next(err);
  }
};
