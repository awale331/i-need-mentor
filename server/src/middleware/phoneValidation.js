const Joi = require("@hapi/joi");
const createError = require('../helper/createError');

const validationSchema = Joi.object({
  phone: Joi.string()
    .pattern(/^\+?(?:[0-9] ?-?){6,14}[0-9]$/) // Can contain '+' at the beginning, followed by numbers. Can have space or '-' in between numbers. Min length 7, max length 15
    .required()
    .error(errors => {
      errors.forEach(err => {
        if (err.code === "string.pattern.base") {
          // Custom error message for regex validation
          err.message = 'Invalid "phone" number';
        }
      });
      return errors;
    })
});

module.exports = (req, res, next) => {
  try {
    const phone = req.path.includes("/add-phone/:id");
    const { error } = validationSchema.validate(req.body, {
      context: { phone }
    });

    if (error) {
      return next(createError(422, error.message));
    }
    return next();
  } catch (err) {
    next(err);
  }
};
