const Joi = require('@hapi/joi');
const createError = require('../helper/createError');

const validationSchema = Joi.object({
  email: Joi.string()
    .email()
    .required(),

  role: Joi.array()
    .items(Joi.string().valid('admin', 'mentor', 'student')),

  password: Joi.string()
    .pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+-=])[A-Za-z\d!@#$%^&*()_+-=s]{8,}$/)
    .error(new Error('Password should have at least 8 characters with at least 1 uppercase, at least 1 lowercase and at least 1 numeric character.')),

  confirmPassword: Joi.any()
    .valid(Joi.ref('password'))
    .required()
    .error(new Error('"confirmPassword" must match with "password"')),

  fullName: Joi.string().required(),

  // phone: Joi.string()
  //   .pattern(/^\+?(?:[0-9] ?-?){6,14}[0-9]$/) // Can contain '+' at the beginning, followed by numbers. Can have space or '-' in between numbers. Min length 7, max length 15
  //   .required()
  //   .error((errors) => {
  //     errors.forEach((err) => {
  //       if (err.code === 'string.pattern.base') { // Custom error message for regex validation
  //         err.message = 'Invalid "phone" number';
  //       }
  //     });
  //     return errors;
  //   }),

  // topics: Joi.array()
  //   .when('userType', { is: 'mentor', then: Joi.array().required() })
});


module.exports = (req, res, next) => {
  try {
    const { error } = validationSchema.validate(req.body);

    if (error) {
      return next(createError(422, error.message));
    }
    return next();
  } catch (err) {
    next(err);
  }
};
