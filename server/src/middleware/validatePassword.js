const Joi = require('@hapi/joi');
const createError = require('../helper/createError');

const validationSchema = Joi.object({
  oldPassword: Joi.string(),

  newPassword: Joi.string()
    .pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d!@#$%^&*()_+]{8,}$/)
    .required()
    .error(new Error('Password should have at least 8 characters with at least 1 uppercase, at least 1 lowercase and at least 1 numeric charcter.')),

  confirmPassword: Joi.any()
    .valid(Joi.ref('newPassword'))
    .required()
    .error(new Error('"confirmPassword" must match with "newPassword"')),
});

module.exports = (req, res, next) => {
  try {
    const { error } = validationSchema.validate(req.body);

    if (error) {
      return next(createError(400, error.message));
    }
    return next();
  } catch (err) {
    next(err);
  }
};
