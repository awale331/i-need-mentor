const createError = require('../helper/createError');

module.exports = (req, res, next) => {
  if (!res.locals.isVerified) {
    next(createError(401, 'Please verify your account first.'));
  } else if (res.locals.role.includes('admin')) {
    next();
  } else if (res.locals.id === req.params.id) {
    next();
  } else {
    next(createError(401, 'Not authorized'));
  }
};
