const jwt = require('jsonwebtoken');

const tokenGenerator = (payload, scope, expiresIn) => {
  console.log(expiresIn)
  return jwt.sign({
    data: payload,
    scope,
  }, process.env.JWT_SECRET, { expiresIn });
};

const accessToken = (payload, scope) => tokenGenerator(payload, scope, process.env.ACCESS_TOKEN_EXP);
const refreshToken = (payload, scope) => tokenGenerator(payload, scope, process.env.REFRESH_TOKEN_EXP);

module.exports = {
  accessToken,
  refreshToken
};
