const nodemailer = require('nodemailer');
const path = require('path');
const EmailTemplates = require('email-templates');

module.exports = async ({ to, subject, template, name, host, token, validity }) => {
  const transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    auth: { user: process.env.EMAIL_USER, pass: process.env.EMAIL_PASSWORD },
  });

  const email = new EmailTemplates({
    send: true,
    message: { from: process.env.EMAIL_USER },
    transport: transporter,
    preview: false,
  });

  await email.send({
    template: path.join(__dirname, '..', 'email', template),
    message: { to, subject },
    locals: { name, host, token, validity },
  });
};
