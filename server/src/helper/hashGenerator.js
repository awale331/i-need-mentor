const bcrypt = require('bcrypt');

const password = process.argv[2];

bcrypt.hash(password, 10, (err, hash) => {
  if (err) {
    console.log(err);
  } else {
    console.log(hash);
  }
});