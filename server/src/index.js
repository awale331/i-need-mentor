const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const logger = require('morgan');
const multer = require('multer');
const createError = require('./helper/createError');
const authentication = require('./routes/authentication');
const users = require('./routes/users');
const categories = require('./routes/categories');

require('dotenv').config({
  path: `${__dirname}/config/.env`,
});

const options = {
  reconnectTries: 30, // Retry up to 30 times
  reconnectInterval: 500, // Reconnect every 500ms
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
}

const connectWithRetry = () => {
  mongoose.connect(process.env.DEV_DB_URL, options).then(() => {
  }).catch(err => {
    console.log('MongoDB connection unsuccessful, retry after 5 seconds.');
    setTimeout(connectWithRetry, 5000);
  });
};
connectWithRetry();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './src/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now())
  }
});

const app = express();

<<<<<<< HEAD
=======
app.use(express.static(`${__dirname}/uploads`));
app.use(express.static(`${__dirname}/public`));
>>>>>>> 6986db7b68f46bd7061173f6e75f8df6120b5f85
app.use(cors());
app.use(logger('dev'));
app.use(multer({ storage }).single('photo'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.send('I need Mentor!')
});

app.use('/api/v1/auth', authentication);
app.use('/api/v1/users', users);
app.use('/api/v1/categories', categories);

app.use((req, res, next) => {
  next(createError(404, 'Not Found'));
});

app.use((err, req, res, next) => {
  console.error(err);
  res.status(err.status || 500).json({
    success: false,
    error: err.message || 'Internal Server Error',
  });
});

app.listen(process.env.SERVER_PORT || 3000, () => {
  console.log(`Server running on port ${process.env.SERVER_PORT || 3000}`);
});
