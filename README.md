# MERN Stack I Need Mentor

## Setup

`1. Clone`<br/>
`2. npm i`<br/>
`3. npm run dev`

## Deploy

```
server {
    listen 80;
    server_name sysmon.tecmint.lan;

    location / {
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header   Host $http_host;
        proxy_pass         http://localhost:port;
    }
}

```

## API DOCS

`
https://documenter.getpostman.com/view/7786137/T17Ke78V?version=latest&fbclid=IwAR2_BaxDzqMiuVo0OlFuphkySzjjgra-qe8u5ox8vJH8FTpH-Wqv3EryHD8#0658455c-ccc6-4d48-8141-4e07c37dbf6d

`
